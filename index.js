(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var camelCase = function camelCase(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function (word, index) {
    return index == 0 ? word.toLowerCase() : word.toUpperCase();
  }).replace(/[\s-]+/g, '');
};

module.exports = {
  camelCase: camelCase
};

},{}],2:[function(require,module,exports){
"use strict"; // Predefine list of available timing functions
// If you need more, tween js is full of great examples
// https://github.com/tweenjs/tween.js/blob/master/src/Tween.js#L421-L737

var easings = {
  // no easing, no acceleration
  linear: function linear(t) {
    return t;
  },
  // accelerating from zero velocity
  easeInQuad: function easeInQuad(t) {
    return t * t;
  },
  // decelerating to zero velocity
  easeOutQuad: function easeOutQuad(t) {
    return t * (2 - t);
  },
  // acceleration until halfway, then deceleration
  easeInOutQuad: function easeInOutQuad(t) {
    return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
  },
  // accelerating from zero velocity
  easeInCubic: function easeInCubic(t) {
    return t * t * t;
  },
  // decelerating to zero velocity
  easeOutCubic: function easeOutCubic(t) {
    return --t * t * t + 1;
  },
  // acceleration until halfway, then deceleration
  easeInOutCubic: function easeInOutCubic(t) {
    return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
  },
  // accelerating from zero velocity
  easeInQuart: function easeInQuart(t) {
    return t * t * t * t;
  },
  // decelerating to zero velocity
  easeOutQuart: function easeOutQuart(t) {
    return 1 - --t * t * t * t;
  },
  // acceleration until halfway, then deceleration
  easeInOutQuart: function easeInOutQuart(t) {
    return t < .5 ? 8 * t * t * t * t : 1 - 8 * --t * t * t * t;
  },
  // accelerating from zero velocity
  easeInQuint: function easeInQuint(t) {
    return t * t * t * t * t;
  },
  // decelerating to zero velocity
  easeOutQuint: function easeOutQuint(t) {
    return 1 + --t * t * t * t * t;
  },
  // acceleration until halfway, then deceleration
  easeInOutQuint: function easeInOutQuint(t) {
    return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * --t * t * t * t * t;
  },
  // elastic bounce effect at the beginning
  easeInElastic: function easeInElastic(t) {
    return (.04 - .04 / t) * Math.sin(25 * t) + 1;
  },
  // elastic bounce effect at the end
  easeOutElastic: function easeOutElastic(t) {
    return .04 * t / --t * Math.sin(25 * t);
  },
  // elastic bounce effect at the beginning and end
  easeInOutElastic: function easeInOutElastic(t) {
    return (t -= .5) < 0 ? (.02 + .01 / t) * Math.sin(50 * t) : (.02 - .01 / t) * Math.sin(50 * t) + 1;
  },
  easeInOutElastic_light: function easeInOutElastic_light(t) {
    var magnitude = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0.01;
    var p = 1 - magnitude;

    if (t === 0 || t === 1) {
      return t;
    }

    var scaledTime = t * 2;
    var scaledTime1 = scaledTime - 1;
    var s = p / (2 * Math.PI) * Math.asin(1);

    if (scaledTime < 1) {
      return -0.5 * (Math.pow(2, 10 * scaledTime1) * Math.sin((scaledTime1 - s) * (2 * Math.PI) / p));
    }

    return Math.pow(2, -10 * scaledTime1) * Math.sin((scaledTime1 - s) * (2 * Math.PI) / p) * 0.5 + 1;
  },
  // Slight acceleration from zero to full speed
  easeInSin: function easeInSin(t) {
    return 1 + Math.sin(Math.PI / 2 * t - Math.PI / 2);
  },
  // Slight deceleration at the end
  easeOutSin: function easeOutSin(t) {
    return Math.sin(Math.PI / 2 * t);
  },
  // Slight acceleration at beginning and slight deceleration at end
  easeInOutSin: function easeInOutSin(t) {
    return (1 + Math.sin(Math.PI * t - Math.PI / 2)) / 2;
  },
  // Accelerate exponentially until finish
  easeInExpo: function easeInExpo(t) {
    return t === 0 ? 0 : Math.pow(2, 10 * (t - 1));
  },
  // Initial exponential acceleration slowing to stop
  easeOutExpo: function easeOutExpo(t) {
    return t === 1 ? 1 : -Math.pow(2, -10 * t) + 1;
  },
  // Exponential acceleration and deceleration
  easeInOutExpo: function easeInOutExpo(t) {
    if (t === 0 || t === 1) {
      return t;
    }

    var scaledTime = t * 2;
    var scaledTime1 = scaledTime - 1;

    if (scaledTime < 1) {
      return 0.5 * Math.pow(2, 10 * scaledTime1);
    }

    return 0.5 * (-Math.pow(2, -10 * scaledTime1) + 2);
  }
};
module.exports = {
  easings: easings
};

},{}],3:[function(require,module,exports){
"use strict"; // scrolls to page sections / anchor tags

var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;

var _require = require('./scrollLinks'),
    scrollTopValue = _require.scrollTopValue,
    scrollLinks = _require.scrollLinks,
    updateLinkPositions = _require.updateLinkPositions,
    updateNavLinkState = _require.updateNavLinkState,
    updateLinks = _require.updateLinks,
    scrollIt = _require.scrollIt;

updateLinkPositions();
scrollLinks.forEach(function (link) {
  link.addEventListener('click', function (e) {
    e.preventDefault();
    var target = document.querySelector(this.hash);

    if (!target) {
      return;
    }

    if (document.getElementsByClassName('mobile-overlay')[0].classList.contains('show')) {
      document.getElementById('mobile-menu-button').click();
    }

    if (this.hash === '#collapseOne') return;
    scrollIt(scrollTopValue(target), 1000, 'easeInOutExpo', function () {
      if (isIE11) {
        target.setActive();
      } else {
        target.focus({
          preventScroll: true
        });
      }
    });
  });
});
setTimeout(function () {
  updateLinks();
}, 1000); // trigger event when window resize ends

var resizeTimer;

window.onresize = function () {
  clearTimeout(resizeTimer);
  resizeTimer = setTimeout(function () {
    updateLinkPositions();
  }, 250);
};

window.addEventListener("scroll", function () {
  var scrollTop = window.pageYOffset;
  updateLinkPositions();
  updateNavLinkState(scrollTop);
}, false);

},{"./scrollLinks":5}],4:[function(require,module,exports){
"use strict";

var _document$body = document.body,
    body_scrollHeight = _document$body.scrollHeight,
    body_offsetHeight = _document$body.offsetHeight;
var _document$documentEle = document.documentElement,
    doc_clientHeight = _document$documentEle.clientHeight,
    doc_scrollHeight = _document$documentEle.scrollHeight,
    doc_offsetHeight = _document$documentEle.offsetHeight,
    docWidth = _document$documentEle.clientWidth;
var _window$document = window.document,
    body = _window$document.body,
    compatMode = _window$document.compatMode;

var documentHeight = function documentHeight() {
  return Math.max(body_scrollHeight, body_offsetHeight, doc_clientHeight, doc_scrollHeight, doc_offsetHeight);
};

var windowHeight = function windowHeight() {
  return window.innerHeight || doc_clientHeight || document.getElementsByTagName('body')[0].clientHeight;
};

var windowWidth = function windowWidth() {
  return compatMode === "CSS1Compat" && docWidth || body && body.clientWidth || docWidth;
};

module.exports = {
  documentHeight: documentHeight,
  windowHeight: windowHeight,
  windowWidth: windowWidth
};

},{}],5:[function(require,module,exports){
"use strict";

var _require = require('./camelCase'),
    camelCase = _require.camelCase;

var _require2 = require('./easings.js'),
    easings = _require2.easings; // Predefine list of available timing functions


var _require3 = require('./measure.js'),
    windowHeight = _require3.windowHeight,
    documentHeight = _require3.documentHeight,
    windowWidth = _require3.windowWidth;

var showMobileMenu = windowWidth <= 767;
var scrollTopValueOffsets = {
  isMobile: {
    navOpen: 330,
    navClosed: {
      isIE11: 55
    }
  }
};

var scrollTopValue = function scrollTopValue(target) {
  var scrollTopValue = 0;
  var targetOffset = target.offsetTop - document.getElementById("nav").clientHeight;

  if (showMobileMenu) {
    var mobileExpanded = document.getElementById('nav-menu').getAttribute('data-expanded');

    if (mobileExpanded === 'true') {
      scrollTopValue = targetOffset - scrollTopValueOffsets.isMobile.navOpen;
    } else {
      scrollTopValue = isIE11 ? targetOffset - scrollTopValueOffsets.isMobile.navClosed.isIE11 : targetOffset;
    }
  } else {
    scrollTopValue = targetOffset;
  }

  return scrollTopValue;
};

var scrollLinks = Array.from(document.querySelectorAll('a[href*="#"]:not([href="#"]):not([href^="http"]):not([href^="#section"])'));
var scrollLinksHash = scrollLinks.map(function (a) {
  return a.href.slice(a.href.indexOf('#') + 1);
});
var scrollLinkOffsets = {};

var updateLinkPositions = function updateLinkPositions() {
  scrollLinkOffsets = {};
  scrollLinksHash.forEach(function (hash) {
    var linkTarget = document.querySelector("#".concat(hash));
    scrollLinkOffsets["".concat(camelCase(hash), "Offset")] = {
      offset: linkTarget.offsetTop ? linkTarget.offsetTop : 0,
      hashText: hash,
      hash: "#".concat(hash)
    };
  });
};

var updateNavLinkState = function updateNavLinkState(scrollTop) {
  var polyfillHeight = 100;

  if (!document.body.classList.contains('index')) {
    return;
  }

  var checkIfScroll = {
    hasPassed: function hasPassed(section) {
      return scrollTop > section - polyfillHeight;
    },
    hasNotReached: function hasNotReached(section) {
      return scrollTop < section - polyfillHeight;
    },
    isBetween: function isBetween(sectionOne, sectionTwo) {
      return this.hasPassed(sectionOne) && this.hasNotReached(sectionTwo);
    }
  }; // apply proper nav highlight when content is shuffled.

  var entries = Object.entries(scrollLinkOffsets);
  var offsetsLength = entries.length;

  var setActiveLink = function setActiveLink(value) {
    document.getElementsByClassName('nav-links')[0].setAttribute('data-active', value[1].hashText);
  };

  entries.forEach(function (value, index) {
    if (index === 0) {
      if (checkIfScroll.hasPassed(value[1].offset)) {
        setActiveLink(value);
      }
    } else if (index === offsetsLength - 1) {
      if (checkIfScroll.hasPassed(value[1].offset)) {
        setActiveLink(value);
      }
    } else {
      if (checkIfScroll.isBetween(value[1].offset, entries[index + 1][1].offset)) {
        setActiveLink(value);
      }
    }
  });
};

var updateLinks = function updateLinks() {
  updateLinkPositions();
  updateNavLinkState(window.pageYOffset);
};

function scrollIt(destination) {
  var duration = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 200;
  var easing = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'linear';
  var callback = arguments.length > 3 ? arguments[3] : undefined; // Store initial position of a window and time
  // If performance is not available in your browser
  // It will fallback to new Date().getTime() - thanks IE < 10

  var start = window.pageYOffset;
  var startTime = 'now' in window.performance ? performance.now() : new Date().getTime(); // const startTime = typeof(window.performance['now']) == 'function' ? performance.now() : new Date().getTime();
  // Take height of window and document to resolve max scrollable value
  // Prevent requestAnimationFrame() from scrolling below maximum scollable value
  // Resolve destination type (node or number)

  var destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop;
  var destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset); // If requestAnimationFrame is not supported
  // Move window to destination position and trigger callback function

  if (!'requestAnimationFrame' in window) {
    window.scroll(0, destinationOffsetToScroll);

    if (callback) {
      callback();
    }

    return;
  } // function resolves position of a window and moves to exact amount of pixels
  // Resolved by calculating delta and timing function chosen by user


  function scroll() {
    var now = 'now' in window.performance ? performance.now() : new Date().getTime();
    var time = Math.min(1, (now - startTime) / duration);
    var timeFunction = easings[easing](time);
    window.scroll(0, Math.ceil(timeFunction * (destinationOffsetToScroll - start) + start)); // Stop requesting animation when window reached its destination
    // And run a callback function

    if (window.pageYOffset === destinationOffsetToScroll) {
      if (callback) callback();
      return;
    } // If window still needs to scroll to reach destination
    // Request another scroll invocation


    requestAnimationFrame(scroll);
  } // Invoke scroll and sequential requestAnimationFrame


  scroll();
}

module.exports = {
  scrollTopValue: scrollTopValue,
  scrollLinks: scrollLinks,
  scrollLinksHash: scrollLinksHash,
  scrollLinkOffsets: scrollLinkOffsets,
  updateLinkPositions: updateLinkPositions,
  updateNavLinkState: updateNavLinkState,
  updateLinks: updateLinks,
  scrollIt: scrollIt
};

},{"./camelCase":1,"./easings.js":2,"./measure.js":4}]},{},[3]);
