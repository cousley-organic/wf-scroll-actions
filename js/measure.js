const {scrollHeight: body_scrollHeight, offsetHeight: body_offsetHeight} = document.body;
const {clientHeight: doc_clientHeight, scrollHeight: doc_scrollHeight, offsetHeight: doc_offsetHeight, clientWidth: docWidth} = document.documentElement;
const {body, compatMode} = window.document;


const documentHeight = () => Math.max(body_scrollHeight, body_offsetHeight, doc_clientHeight, doc_scrollHeight, doc_offsetHeight);
const windowHeight = () => window.innerHeight || doc_clientHeight || document.getElementsByTagName('body')[0].clientHeight;
const windowWidth = () => compatMode === "CSS1Compat" && docWidth || body && body.clientWidth || docWidth;

module.exports = {
	documentHeight: documentHeight,
	windowHeight: windowHeight,
	windowWidth: windowWidth,
};
