const {camelCase} = require('./camelCase');
const {easings} = require('./easings.js'); // Predefine list of available timing functions
const {windowHeight, documentHeight, windowWidth} = require('./measure.js');
const showMobileMenu = windowWidth <= 767;
const scrollTopValueOffsets = {
	isMobile: {
		navOpen: 330,
		navClosed: {
			isIE11: 55
		}
	},
};

const scrollTopValue = function (target) {
	let scrollTopValue = 0;
	const targetOffset = target.offsetTop - document.getElementById("nav").clientHeight;

	if (showMobileMenu) {
		const mobileExpanded = document.getElementById('nav-menu').getAttribute('data-expanded');
		if (mobileExpanded === 'true') {
			scrollTopValue = (targetOffset - scrollTopValueOffsets.isMobile.navOpen);
		}
		else {
			scrollTopValue = (isIE11) ? (targetOffset - scrollTopValueOffsets.isMobile.navClosed.isIE11) : targetOffset;
		}
	}
	else {
		scrollTopValue = targetOffset;
	}

	return scrollTopValue;

};
const scrollLinks = Array.from(document.querySelectorAll('a[href*="#"]:not([href="#"]):not([href^="http"]):not([href^="#section"])'));
const scrollLinksHash = scrollLinks.map((a) => a.href.slice(a.href.indexOf('#') + 1));
let scrollLinkOffsets = {};

const updateLinkPositions = function () {
	scrollLinkOffsets = {};
	scrollLinksHash.forEach(hash => {
		const linkTarget = document.querySelector(`#${hash}`);
		scrollLinkOffsets[`${camelCase(hash)}Offset`] = {
			offset: linkTarget.offsetTop ? linkTarget.offsetTop : 0,
			hashText: hash,
			hash: `#${hash}`
		}
	})
};
const updateNavLinkState = function (scrollTop) {
	const polyfillHeight = 100;
	if (!document.body.classList.contains('index')) {
		return;
	}

	const checkIfScroll = {
		hasPassed: (section) => scrollTop > section - polyfillHeight,
		hasNotReached: (section) => scrollTop < section - polyfillHeight,
		isBetween: function (sectionOne, sectionTwo) {
			return (this.hasPassed(sectionOne) && this.hasNotReached(sectionTwo));
		}
	};

	// apply proper nav highlight when content is shuffled.

	let entries = Object.entries(scrollLinkOffsets);
	let offsetsLength = entries.length;
	const setActiveLink = (value) => {
		document.getElementsByClassName('nav-links')[0].setAttribute('data-active', value[1].hashText);
	};

	entries.forEach((value, index) => {
		if (index === 0) {
			if (checkIfScroll.hasPassed(value[1].offset)) {
				setActiveLink(value);
			}
		}
		else if (index === (offsetsLength - 1)) {
			if (checkIfScroll.hasPassed(value[1].offset)) {
				setActiveLink(value);
			}
		}
		else {
			if (checkIfScroll.isBetween(value[1].offset, entries[index + 1][1].offset)) {
				setActiveLink(value);
			}
		}
	});

};
const updateLinks = function () {
	updateLinkPositions();
	updateNavLinkState(window.pageYOffset);
};
function scrollIt(destination, duration = 200, easing = 'linear', callback) {

	// Store initial position of a window and time
	// If performance is not available in your browser
	// It will fallback to new Date().getTime() - thanks IE < 10
	const start = window.pageYOffset;
	const startTime = 'now' in window.performance ? performance.now() : new Date().getTime();
	// const startTime = typeof(window.performance['now']) == 'function' ? performance.now() : new Date().getTime();


	// Take height of window and document to resolve max scrollable value
	// Prevent requestAnimationFrame() from scrolling below maximum scollable value
	// Resolve destination type (node or number)
	const destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop;
	const destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);


	// If requestAnimationFrame is not supported
	// Move window to destination position and trigger callback function
	if (!'requestAnimationFrame' in window) {
		window.scroll(0, destinationOffsetToScroll);
		if (callback) {
			callback();
		}
		return;
	}


	// function resolves position of a window and moves to exact amount of pixels
	// Resolved by calculating delta and timing function chosen by user
	function scroll() {
		const now = 'now' in window.performance ? performance.now() : new Date().getTime();
		const time = Math.min(1, ((now - startTime) / duration));
		const timeFunction = easings[easing](time);
		window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));

		// Stop requesting animation when window reached its destination
		// And run a callback function
		if (window.pageYOffset === destinationOffsetToScroll) {
			if (callback)
				callback();
			return;
		}

		// If window still needs to scroll to reach destination
		// Request another scroll invocation
		requestAnimationFrame(scroll);
	}


	// Invoke scroll and sequential requestAnimationFrame
	scroll();
}


module.exports = {
	scrollTopValue: scrollTopValue,
	scrollLinks: scrollLinks,
	scrollLinksHash: scrollLinksHash,
	scrollLinkOffsets: scrollLinkOffsets,
	updateLinkPositions: updateLinkPositions,
	updateNavLinkState: updateNavLinkState,
	updateLinks: updateLinks,
	scrollIt: scrollIt,
};
