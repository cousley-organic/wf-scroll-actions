// Predefine list of available timing functions
// If you need more, tween js is full of great examples
// https://github.com/tweenjs/tween.js/blob/master/src/Tween.js#L421-L737

const easings = {
	// no easing, no acceleration
	linear: (t) => t,
	// accelerating from zero velocity
	easeInQuad: (t) => t * t,
	// decelerating to zero velocity
	easeOutQuad: (t) => t * (2 - t),
	// acceleration until halfway, then deceleration
	easeInOutQuad: (t) => t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t,
	// accelerating from zero velocity
	easeInCubic: (t) => t * t * t,
	// decelerating to zero velocity
	easeOutCubic: (t) => (--t) * t * t + 1,
	// acceleration until halfway, then deceleration
	easeInOutCubic: (t) => t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1,
	// accelerating from zero velocity
	easeInQuart: (t) => t * t * t * t,
	// decelerating to zero velocity
	easeOutQuart: (t) => 1 - (--t) * t * t * t,
	// acceleration until halfway, then deceleration
	easeInOutQuart: (t) => t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t,
	// accelerating from zero velocity
	easeInQuint: (t) => t * t * t * t * t,
	// decelerating to zero velocity
	easeOutQuint: (t) => 1 + (--t) * t * t * t * t,
	// acceleration until halfway, then deceleration
	easeInOutQuint: (t) => t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t,

	// elastic bounce effect at the beginning
	easeInElastic: (t) => (.04 - .04 / t) * Math.sin(25 * t) + 1,
	// elastic bounce effect at the end
	easeOutElastic: (t) => .04 * t / (--t) * Math.sin(25 * t),
	// elastic bounce effect at the beginning and end
	easeInOutElastic: (t) => (t -= .5) < 0 ? (.02 + .01 / t) * Math.sin(50 * t) : (.02 - .01 / t) * Math.sin(50 * t) + 1,

	easeInOutElastic_light: (t, magnitude = 0.01) => {

		const p = 1 - magnitude;

		if (t === 0 || t === 1) {
			return t;
		}

		const scaledTime = t * 2;
		const scaledTime1 = scaledTime - 1;

		const s = p / (2 * Math.PI) * Math.asin(1);

		if (scaledTime < 1) {
			return -0.5 * (
				Math.pow(2, 10 * scaledTime1) *
				Math.sin((scaledTime1 - s) * (2 * Math.PI) / p)
			);
		}

		return (
			Math.pow(2, -10 * scaledTime1) *
			Math.sin((scaledTime1 - s) * (2 * Math.PI) / p) * 0.5
		) + 1;

	},
	// Slight acceleration from zero to full speed
	easeInSin: (t) => 1 + Math.sin(Math.PI / 2 * t - Math.PI / 2),
	// Slight deceleration at the end
	easeOutSin: (t) => Math.sin(Math.PI / 2 * t),
	// Slight acceleration at beginning and slight deceleration at end
	easeInOutSin: (t) => (1 + Math.sin(Math.PI * t - Math.PI / 2)) / 2,

	// Accelerate exponentially until finish
	easeInExpo: (t) => (t === 0) ? 0 : Math.pow(2, 10 * (t - 1)),
	// Initial exponential acceleration slowing to stop
	easeOutExpo: (t) => (t === 1) ? 1 : (-Math.pow(2, -10 * t) + 1),
	// Exponential acceleration and deceleration
	easeInOutExpo: (t) => {

		if (t === 0 || t === 1) {
			return t;
		}

		const scaledTime = t * 2;
		const scaledTime1 = scaledTime - 1;

		if (scaledTime < 1) {
			return 0.5 * Math.pow(2, 10 * (scaledTime1));
		}

		return 0.5 * (-Math.pow(2, -10 * scaledTime1) + 2);

	},

}
module.exports = {

	easings: easings

};
