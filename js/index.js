// scrolls to page sections / anchor tags

const isIE11 = !!window.MSInputMethodContext && !!document.documentMode;



const {scrollTopValue, scrollLinks, updateLinkPositions, updateNavLinkState, updateLinks, scrollIt} = require('./scrollLinks');


updateLinkPositions();

scrollLinks.forEach(function (link) {
	link.addEventListener('click', function (e) {
		e.preventDefault();
		const target = document.querySelector(this.hash);

		if (!target) {
			return;
		}

		if (document.getElementsByClassName('mobile-overlay')[0].classList.contains('show')) {
			document.getElementById('mobile-menu-button').click();
		}


		if (this.hash === '#collapseOne') return;

		scrollIt(
			scrollTopValue(target),
			1000,
			'easeInOutExpo',
			() => {
				if (isIE11) {
					target.setActive();
				}
				else {
					target.focus({preventScroll: true});
				}
			}
		);
	})
});

setTimeout(() => {
	updateLinks()
}, 1000);


// trigger event when window resize ends
let resizeTimer;

window.onresize = function () {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(() => {
		updateLinkPositions();
	}, 250);
};


window.addEventListener("scroll", function () {
	const scrollTop = window.pageYOffset;
	updateLinkPositions();
	updateNavLinkState(scrollTop);
}, false);

